//
//  main.m
//  TwoFloatss
//
//  Created by Admin on 19.10.15.
//  Copyright (c) 2015 Valeriya Mozgovaya. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        float firstValue = 3.42;
        float secondValue = 6.31;
        double result = firstValue + secondValue;
        NSLog(@"Sum of firstValue %.2f and secondValue %.2f = %.2f", firstValue, secondValue, result);
    }
    return 0;
}
